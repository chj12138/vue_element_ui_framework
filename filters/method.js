/**
 * @description 补零函数
 * @param {*} value
 */
const paddingZero = function(value) {
  return value < 10 ? '0' + value : value
}

/**
 * @description 时间格式化
 * @argument time/ms
 * @return YYYY-MM-DD HH:mm:ss || '--'
 */
const formatDateTime = function(time) {
  let timeString = new Date(time)
  return (
    timeString.getFullYear() +
    '-' +
    paddingZero(timeString.getMonth() + 1) +
    '-' +
    paddingZero(timeString.getDate()) +
    ' ' +
    paddingZero(timeString.getHours()) +
    ':' +
    paddingZero(timeString.getMinutes()) +
    ':' +
    paddingZero(timeString.getSeconds())
  )
}

/**
 * @description 手机号格式化
 * @argument phone
 * @return /*** **** ****
 */
const formatPhone = function(phone) {
  return Array.from(phone, (a, b) => {
    // Array.from(string, map(a => item, b => index)) String to Array
    if (b === 2 || b === 6) {
      a += ' '
    }
    return a
  }).join('') // join => Array to String
}

/**
 *
 * @description 映射实际值 注:所有映射的map必须为{label:'',value:''}的对象数组
 * @param {*} obj
 * @param {*} map
 * @returns
 */
const mappingObj = function(obj, map) {
  // TODO:当obj是数值时，Array.from函数会生成一个空数组
  // TODO:当前端Array.from中的参数为长度大于1的字符串，会对他自动进行分割转化成数组
  // TODO: 先将传入的值转化成一个数组
  // FIXME: 由于后端返回的接单类别前后都有逗号,切割时会出现值为''的值,需要将之过滤
  if (!map) map = []
  obj =
    typeof obj === 'number'
      ? [obj]
      : typeof obj === 'string'
        ? obj.split(',').filter(item => item && item.length > 0)
        : obj
  return (
    (obj &&
      map &&
      obj
        .map(
          it =>
            (map.filter(item => item.value === Number(it))[0] &&
              map.filter(item => item.value === Number(it))[0].label) ||
            '--'
        )
        .join(',')) ||
    '--'
  )
}

/**
 *
 * @param {} obj
 * @param {number} 最大长度
 */
const maxLength = function(obj, ext) {
  return (obj.length > ext && obj.substr(0, ext) + '...') || obj
}

/**
 * 完成时限 ms 转小时
 * @param {time} obj
 */
const msToH = function(obj) {
  return Number(obj) !== 0 ? (obj / 3600000).toFixed(0) + 'h' : '不限'
}

export default {
  formatDateTime,
  formatPhone,
  mappingObj,
  maxLength,
  msToH
}
