import tableFilters from './method'




 export const yui = function(v) {
  if(v) {
		return v;
	}else{
		return '--'
	}
}

const getValue = function(row, obj) {
  let median = row
  if (typeof obj === 'string') {
    return median[obj]
  } else {
    obj.forEach(item => {
      median = median[obj]
    })
  }
  return median
}


/* 抽奖活动状态过滤器 */

/*行程状态*/
export function StatusFilter(status) {
  var result = null
  switch (status) {
    case 1:
      result = '已预约'
      break
    case 2:
      result = '行程中'
      break
    case 3:
      result = '已完成'
      break
    case 4:
      result = '已取消'
      break
    default:
      result = '--'
  }
  return result
}
/*订单类型*/
export function StatusTypeFilter(status) {
  var result = null
  switch (status) {
    case 1:
      result = '保养'
      break
    case 2:
      result = '年检'
      break
    case 4:
      result = '巡检'
      break
    case 8:
      result = '清洁'
      break
    case 16:
      result = '调度'
      break
    case 32:
      result = '违章'
      break
    case 64:
      result = '事故'
      break
    default:
      result = '--'
  }
  return result
}

export function SpeialRecStatus(status) {
  var result = null
  switch (status) {
    case 1:
      result = '待处理'
      break
    case 2:
      result = '已处理'
      break
    default:
      result = '--'
  }
  return result
}

export function SpeialStatus(status) {
  var result = null
  switch (status) {
    case 1:
      result = '营运中'
      break
    case 2:
      result = '待运中'
      break
    case 3:
      result = '保养停运'
      break
    case 4:
      result = '年检停运'
      break
    case 5:
      result = '调度停运'
      break
    case 6:
      result = '事故停运'
      break
    case 7:
      result = '异常停运'
      break
    case 8:
      result = '报废停运'
      break
    case 9:
      result = '特请停运'
      break
    case 10:
      result = '其它原因停运'
      break
    default:
      result = '--'
  }
  return result
}
/*年检状态*/
export function annualStatus(status) {
  var result = null
  switch (status) {
    case 1:
      result = '无需年检'
      break
    case 2:
      result = '年检待处理'
      break
    case 3:
      result = '年检已开始'
      break
    case 4:
      result = '年检已完成'
      break
    default:
      result = '--'
  }
  return result
}
/*年检到期状态*/
export function annualExpireStatus(status) {
  var result = null
  switch (status) {
    case 0:
      result = '年检未到期'
      break
    case 1:
      result = '年检已到期'
      break
    default:
      result = '--'
  }
  return result
}
/*操作类别*/
export function oPerateyType(status) {
  var result = null
  switch (status) {
    case 0:
      result = '取消派单'
      break
    case 1:
      result = '派单'
      break
    default:
      result = '--'
  }
  return result
}
/*支付状态*/
export function payStatus(status) {
  var result = null
  switch (status) {
    case 1:
      result = '未支付'
      break
    case 2:
      result = '已支付'
      break
    case 3:
      result = '无需支付'
      break
    default:
      result = '--'
  }
  return result
}
/*租车订单操作类型*/
export function rentTripType(status) {
  var result = null
  switch (status) {
    case 1:
      result = '办事员接单创建'
      break
    case 2:
      result = '后台直接派单'
      break
    default:
      result = '--'
  }
  return result
}
/*行程结束方式*/
export function rentTripEndway(status) {
  var result = null
  switch (status) {
    case 1:
      result = '人工结束'
      break
    case 2:
      result = '正常结束'
      break
    default:
      result = '--'
  }
  return result
}
/*订单类型*/
export function accmanStatus(status) {
  var result = null
  switch (status) {
    case 1:
      result = '维修'
      break
    case 2:
      result = '拖车'
      break
    case 3:
      result = '其它'
      break
    default:
      result = '--'
  }
  return result
}
/*订单状态*/
export function OrderStatus(status) {
  var result = null
  switch (status) {
    case 1:
      result = '待派单'
      break
    case 2:
      result = '已派单'
      break
    case 3:
      result = '已完成'
      break
    case 4:
      result = '已取消'
      break
    default:
      result = '--'
  }
  return result
}
/*租车订单状态*/
export function rentOrderStatus(status) {
  var result = null
  switch (status) {
    case 1:
      result = '已创建'
      break
    case 2:
      result = '已取消'
      break
    case 3:
      result = '已完成'
      break
    default:
      result = '--'
  }
  return result
}
/*派单订单状态*/
export function accqueStatus(status) {
  var result = null
  switch (status) {
    case 1:
      result = '供应商待接单'
      break
    case 2:
      result = '供应商取消派单'
      break
    case 3:
      result = '系统取消'
      break
    case 4:
      result = '供应商已接单'
      break
    case 5:
      result = '办事员拒接单'
      break
    case 6:
      result = '办事员待接单'
      break
    case 7:
      result = '办事员已接单'
      break
    case 8:
      result = '办事员已提交'
      break
    case 9:
      result = '办事员被驳回'
      break
    case 10:
      result = '供应商已完成'
      break
    case 11:
      result = '供应商拒接单'
      break
    case 12:
      result = '后台取消'
      break
    case 13:
      result = '供应商已完成-未还车'
      break
    case 14:
      result = '供应商已完成-超时-未还车'
      break
    case 15:
      result = '供应商已完成-超时'
      break
    default:
      result = '--'
  }
  return result
}

export function strFormat(val) {
  if (val == null || (val + '').trim() == '') {
    return '--' //"— —"
  }
  return val
}

// 将数字转换成货币格式
export function formatMoney(num) {
  if ((num === null || num === '' || num === '--') && num !== 0) {
    return '--'
  } else {
    num += ''
    num = num.replace(/[^0-9|\.]/g, '') //清除字符串中的非数字非.字符

    if (/^0+/)
      //清除字符串开头的0
      num = num.replace(/^0+/, '')
    if (!/\./.test(num))
      //为整数字符串在末尾添加.00
      num += '.00'
    if (/^\./.test(num))
      //字符以.开头时,在开头添加0
      num = '0' + num
    num += '00' //在字符串末尾补零
    num = num.match(/\d+\.\d{2}/)[0]
    return num
  }
}

export function LongToHM(s) {
  if (s == null || s < 0) {
    return '--'
  } else {
    var hour = Math.floor(s / 3600000)
    var min = Math.ceil(s / 60000) % 60
    return hour + '小时' + min + '分钟'
  }
}

export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return '--'
  }

  if (time == null || time === undefined || time.length === 0) {
    return '--'
  }

  if ((time + '').length === 10) {
    time = +time * 1000
  }

  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time == 'object') {
    date = time
  } else {
    date = new Date(parseInt(time))
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    if (key === 'a')
      return ['一', '二', '三', '四', '五', '六', '日'][value - 1]
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}
